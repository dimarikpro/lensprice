class Lensitem < ActiveRecord::Base
  include Commentable

  belongs_to :user
  belongs_to :lensmodel

  validates :price, presence: true, numericality:  { greater_than: 0 }
  validates :lensmodel_id, presence: true
  validates :user_id, presence: true
  # validates :description, length: { minimum: 2, maximum: 1000 }

  after_create  { increase_quantity }
  after_destroy { decrease_quantity }

  mount_uploader :image, ImageUploader

  private

  def increase_quantity
    lensmodel.update_attributes(quantity: lensmodel.quantity + 1)
  end

  def decrease_quantity
    lensmodel.update_attributes(quantity: lensmodel.quantity - 1)
  end
end
