class Cameraitem < ActiveRecord::Base
  include Commentable

  belongs_to :user
  belongs_to :cameramodel

  validates :price, presence: true, numericality:  { greater_than: 0 }
  validates :cameramodel_id, presence: true
  validates :user_id, presence: true
  # validates :description, length: { minimum: 2, maximum: 1000 }

  after_create  { increase_quantity }
  after_destroy { decrease_quantity }

  mount_uploader :image, ImageUploader

  private

  def increase_quantity
    cameramodel.update_attributes(quantity: cameramodel.quantity + 1)
  end

  def decrease_quantity
    cameramodel.update_attributes(quantity: cameramodel.quantity - 1)
  end
end
