class Comment < ActiveRecord::Base
  belongs_to :commentable, polymorphic: true
  belongs_to :user
  has_many :attachments

  validates :body, presence: true, length: { minimum: 2, maximum: 1000 }
  validates :user_id, presence: true
  validates :commentable, presence: true

  scope :sorted_comments, -> { order('created_at') }

  accepts_nested_attributes_for :attachments
end
