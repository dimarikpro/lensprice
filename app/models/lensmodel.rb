class Lensmodel < ActiveRecord::Base
  include Commentable
  has_many :lensitems, foreign_key: 'lensmodel_id', dependent: :destroy

  validates :brand,   presence: true, length:        { minimum: 2, maximum: 15 }
  validates :name,    presence: true, length:        { minimum: 2, maximum: 60 }
  validates :mount,   presence: true, length:        { minimum: 2, maximum: 10 }
  validates :frmin,   presence: true, numericality:  { greater_than: 0, less_than: 2000 }
  validates :frmax,   presence: true, numericality:  { greater_than: 0, less_than: 2000 }
  validates :diafmin, presence: true, numericality:  { greater_than: 0, less_than: 20 }
  validates :diafmax, presence: true, numericality:  { greater_than: 0, less_than: 20 }

  scope :only_sony,    -> { where(brand: 'SONY').order("frmin") }
  scope :only_samyang, -> { where(brand: 'Samyang') }
  scope :only_sigma,   -> { where(brand: 'Sigma') }
  scope :only_tamron,  -> { where(brand: 'Tamron') }
  scope :only_cz,      -> { where(brand: 'Carl Zeiss') }
  scope :only_minolta, -> { where(brand: 'MINOLTA') }
  scope :only_ff,      -> { where(ff: true) }
  scope :only_crop,    -> { where(ff: false) }
  scope :only_fix,     -> { where(fix: true) }
  scope :only_tele,    -> { where(fix: false) }
  scope :only_A,       -> { where(mount: 'A-mount') }
  scope :only_E,       -> { where(mount: 'E-mount') }

  def fullname
    "#{mount} #{brand} #{name}"
  end
end
