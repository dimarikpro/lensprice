class Cameramodel < ActiveRecord::Base
  include Commentable
  has_many :cameraitems, foreign_key: 'cameramodel_id', dependent: :destroy

  validates :brand, presence: true, length: { minimum: 2, maximum: 15 }
  validates :name,  presence: true, length: { minimum: 1, maximum: 60 }
  validates :mount, presence: true, length: { minimum: 2, maximum: 10 }

  scope :only_a, -> { where(mount: 'A-mount').order('name') }
  scope :only_e, -> { where(mount: 'E-mount').order('name') }

  def fullname
    "#{brand} #{name}"
  end
end
