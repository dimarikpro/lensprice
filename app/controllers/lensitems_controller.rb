class LensitemsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :load_lensitem, only: [:show, :edit, :update, :destroy]

  authorize_resource
  # respond_to :js, :json

  def index
    respond_with(@lensitems = Lensitem.all)
  end

  def show
    @lensmodel = @lensitem.lensmodel
    respond_with @lensitem
  end

  def new
    @lensmodels = Lensmodel.order("frmin")
    respond_with(@lensitem = Lensitem.new)
  end

  def edit
  end

  def create
    respond_with(@lensitem = Lensitem.create(lensitem_params))
  end

  def update
    @lensitem.update(lensitem_params)
    respond_with @lensitem
  end

  def destroy
    @lensitem.destroy if @lensitem.user_id == current_user.id
    respond_with @lensitem
  end

  private

  def load_lensitem
    @lensitem = Lensitem.find(params[:id])
  end

  def lensitem_params
    params.require(:lensitem).permit(:lensmodel_id, :price, :description, :image, :used).merge(user_id: current_user.id)
  end
end
