class CameraitemsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :load_cameraitem, only: [:show, :edit, :update, :destroy]

  authorize_resource
  # respond_to :js, :json

  def index
    respond_with(@cameraitems = Cameraitem.all)
  end

  def show
    @cameramodel = @cameraitem.cameramodel
    respond_with @cameraitem
  end

  def new
    @cameramodels = Cameramodel.order("name")
    respond_with(@cameraitem = Cameraitem.new)
  end

  def edit
  end

  def create
    respond_with(@cameraitem = Cameraitem.create(cameraitem_params))
  end

  def update
    @cameraitem.update(cameraitem_params)
    respond_with @cameraitem
  end

  def destroy
    @cameraitem.destroy if @cameraitem.user_id == current_user.id
    respond_with @cameraitem
  end

  private

  def load_cameraitem
    @cameraitem = Cameraitem.find(params[:id])
  end

  def cameraitem_params
    params.require(:cameraitem).permit(:cameramodel_id, :price, :description, :image, :used).merge(user_id: current_user.id)
  end
end
