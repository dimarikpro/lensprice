class AttachmentsController < ApplicationController
  before_action :authenticate_user!
  authorize_resource

  def destroy
    @attachment = Attachment.find(params[:id])
    #authorize! :destroy, @attachment
    temp = Comment.find(@attachment.comment_id)
    @attachment.destroy if current_user.id == temp.user_id
  end
end
