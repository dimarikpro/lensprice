class LensmodelsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :load_lensmodel, only: [:show, :edit, :update, :destroy]

  authorize_resource
  # respond_to :js, :json

  def index
    @cameramodels = Cameramodel.all
    respond_with(@lensmodels = Lensmodel.all)
  end

  def show
    @lensitems = @lensmodel.lensitems
    respond_with @lensmodel
  end

  def new
    respond_with(@lensmodel = Lensmodel.new)
  end

  def edit
  end

  def create
    respond_with(@lensmodel = Lensmodel.create(lensmodel_params))
  end

  def update
    @lensmodel.update(lensmodel_params)
    respond_with @lensmodel
  end

  def destroy
    respond_with(@lensmodel.destroy)
  end

  private

  def load_lensmodel
    @lensmodel = Lensmodel.find(params[:id])
  end

  def lensmodel_params
    params.require(:lensmodel).permit(:name, :brand, :mount, :frmin, :frmax, :diafmin, :diafmax, :ff, :fix, :afdrive, :macro, :weather, :link1, :link2)
  end
end
