require 'application_responder'

class ApplicationController < ActionController::Base
  self.responder = ApplicationResponder
  respond_to :html

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |e|
    redirect_to root_url, alert: e.message
  end

  check_authorization :unless => :do_not_check_authorization?
  private
  def do_not_check_authorization?
    respond_to?(:devise_controller?) or
        condition_one? or
        condition_two?
  end

  def condition_two?
  end

  def condition_one?
  end
end
