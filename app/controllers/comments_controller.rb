class CommentsController < ApplicationController
  before_action :authenticate_user!
  before_action :load_parent, only: :create
  before_action :load_comment, only: [:update, :destroy]
  #  skip_before_filter :verify_authenticity_token
  authorize_resource
  respond_to :js

  def create
    @comment = @parent.comments.create(comment_params)
    respond_with(@comment, location: -> { polymorphic_path(@parent) })
  end

  def update
    @comment.update(comment_params)
    respond_with @comment
  end

  def destroy
    respond_with (@comment.destroy)
  end

  private

  def load_comment
    @comment = Comment.find(params[:id])
  end

  def load_parent
    @parent = Lensmodel.find(params[:lensmodel_id]) if params[:lensmodel_id]
    @parent = Cameramodel.find(params[:cameramodel_id]) if params[:cameramodel_id]
    @parent = Cameraitem.find(params[:cameraitem_id]) if params[:cameraitem_id]
    @parent = Lensitem.find(params[:lensitem_id]) if params[:lensitem_id]
    @parent ||= User.find(params[:user_id])
  end

  def comment_params
    params.require(:comment).permit(:body, attachments_attributes: [:file]).merge(user_id: current_user.id)
  end
end
