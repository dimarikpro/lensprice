class CameramodelsController < ApplicationController
  before_action :authenticate_user!, except: [:index, :show]
  before_action :load_cameramodel, only: [:show, :edit, :update, :destroy]

  authorize_resource
  # respond_to :js, :json

  def index
    @camera_a = Cameramodel.only_a
    @camera_e = Cameramodel.only_e
    respond_with(@cameramodels = Cameramodel.all)
  end

  def show
    @cameraitems = @cameramodel.cameraitems
    respond_with @cameramodel
  end

  def new
    respond_with(@cameramodel = Cameramodel.new)
  end

  def edit
  end

  def create
    respond_with(@cameramodel = Cameramodel.create(cameramodel_params))
  end

  def update
    @cameramodel.update(cameramodel_params)
    respond_with @cameramodel
  end

  def destroy
    respond_with(@cameramodel.destroy)
  end

  private

  def load_cameramodel
    @cameramodel = Cameramodel.find(params[:id])
  end

  def cameramodel_params
    params.require(:cameramodel).permit(:name, :brand, :mount, :frmin, :frmax, :diafmin, :diafmax, :ff, :fix, :afdrive, :macro, :weather, :link1, :link2)
  end
end
