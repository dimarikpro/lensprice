require 'rails_helper'

RSpec.describe LensmodelsController, type: :controller do
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }
  let(:lensmodels) { create_list(:lensmodel, 5) }
  let(:lensmodel) { create(:lensmodel) }
  let(:lensitem1) { create(:lensitem, lensmodel: lensmodel, user: user1) }
  let(:lensitem2) { create(:lensitem, lensmodel: lensmodel, user: user2) }

  describe 'GET index' do
    before { get :index }
    it 'return array of all @lensmodels' do
      expect(assigns(:lensmodels)).to match_array(lensmodels)
    end
    it 'render index view' do
      expect(response).to render_template :index
    end
  end

  describe 'GET show' do
    before { get :show, id: lensmodel }
    it 'return @lensmodel' do
      expect(assigns(:lensmodel)).to eq lensmodel
    end

    it 'return new @lensitems for lensmodel' do
      expect(assigns(:lensitems)).to match_array([lensitem1, lensitem2])
    end

    it 'render show view' do
      expect(response).to render_template :show
    end
  end

  describe 'GET new' do
    sign_in_admin
    before { get :new }
    it 'new lensmodel to @lensmodel' do
      expect(assigns(:lensmodel)).to be_a_new(Lensmodel)
    end

    it 'render new view' do
      expect(response).to render_template :new
    end
  end

  describe 'GET edit' do
    sign_in_admin
    before { get :edit, id: lensmodel }
    it 'lensmodel to @lensmodel' do
      expect(assigns(:lensmodel)).to eq lensmodel
    end

    it 'render edit view' do
      expect(response).to render_template :edit
    end
  end

  describe 'POST create' do
    sign_in_admin
    context 'valid attributes' do
      it 'increase Lensmodel after create' do
        expect { post :create, lensmodel: attributes_for(:lensmodel) }.to change(Lensmodel, :count).by(1)
      end

      it 'redirect to lensmodel/:id' do
        post :create, lensmodel: attributes_for(:lensmodel)
        expect(response).to redirect_to lensmodel_path(assigns(:lensmodel))
      end
    end

    context 'invalid attributes' do
      let(:invalid_lensmodel) { create(:invalid_lensmodel) }
      it 'not increase Lensmodel after create' do
        expect { post :create, lensmodel: attributes_for(:invalid_lensmodel) }.to_not change(Lensmodel, :count)
      end

      it 'render new view' do
        post :create, lensmodel: attributes_for(:invalid_lensmodel)
        expect(response).to render_template :new
      end
    end
  end

  describe 'PATCH update' do
    sign_in_admin
    context 'valid attributes' do
      it 'return lensmodel to @lensmodel' do
        patch :update, id: lensmodel, lensmodel: attributes_for(:lensmodel)
        expect(assigns(:lensmodel)).to eq lensmodel
      end

      it 'changes lensmodel attributes' do
        patch :update, id: lensmodel, lensmodel: { name: 'new name', brand: 'new brand' }
        lensmodel.reload
        expect(lensmodel.name).to eq 'new name'
        expect(lensmodel.brand).to eq 'new brand'
      end

      it 'redirect to show view' do
        patch :update, id: lensmodel, lensmodel: attributes_for(:lensmodel)
        expect(response).to redirect_to lensmodel
      end
    end

    context 'invalid attributes' do
      let(:lensmodel1) { create(:lensmodel, name: '18-55') }
      before { patch :update, id: lensmodel1, lensmodel: { name: nil, brand: nil } }
      it 'doesnt change lensmodel attributes' do
        expect(lensmodel1.name).to eq '18-55'
        expect(lensmodel1.brand).to eq 'SONY'
      end

      it 'render edit view' do
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE destroy' do
    sign_in_admin
    let!(:lensmodel1) { create(:lensmodel) }
    # before { delete :destroy, id: lensmodel1 }
    it 'delete lensmodel' do
      expect { delete :destroy, id: lensmodel1 }.to change(Lensmodel, :count).by(-1)
    end

    it 'redirect to lensmodel/:id' do
      delete :destroy, id: lensmodel1
      expect(response).to redirect_to lensmodels_path
    end
  end
end
