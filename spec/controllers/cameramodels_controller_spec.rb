require 'rails_helper'

RSpec.describe CameramodelsController, type: :controller do
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }
  let(:cameramodels) { create_list(:cameramodel, 5) }
  let(:cameramodel) { create(:cameramodel) }
  let(:cameraitem1) { create(:cameraitem, cameramodel: cameramodel, user: user1) }
  let(:cameraitem2) { create(:cameraitem, cameramodel: cameramodel, user: user2) }

  describe 'GET index' do
    before { get :index }
    it 'return array of all @cameramodels' do
      expect(assigns(:cameramodels)).to match_array(cameramodels)
    end
    it 'render index view' do
      expect(response).to render_template :index
    end
  end

  describe 'GET show' do
    before { get :show, id: cameramodel }
    it 'return @cameramodel' do
      expect(assigns(:cameramodel)).to eq cameramodel
    end

    it 'return new @cameraitems for cameramodel' do
      expect(assigns(:cameraitems)).to match_array([cameraitem1, cameraitem2])
    end

    it 'render show view' do
      expect(response).to render_template :show
    end
  end

  describe 'GET new' do
    sign_in_admin
    before { get :new }
    it 'new cameramodel to @cameramodel' do
      expect(assigns(:cameramodel)).to be_a_new(Cameramodel)
    end

    it 'render new view' do
      expect(response).to render_template :new
    end
  end

  describe 'GET edit' do
    sign_in_admin
    before { get :edit, id: cameramodel }
    it 'cameramodel to @cameramodel' do
      expect(assigns(:cameramodel)).to eq cameramodel
    end

    it 'render edit view' do
      expect(response).to render_template :edit
    end
  end

  describe 'POST create' do
    sign_in_admin
    context 'valid attributes' do
      it 'increase Cameramodel after create' do
        expect { post :create, cameramodel: attributes_for(:cameramodel) }.to change(Cameramodel, :count).by(1)
      end

      it 'redirect to cameramodel/:id' do
        post :create, cameramodel: attributes_for(:cameramodel)
        expect(response).to redirect_to cameramodel_path(assigns(:cameramodel))
      end
    end

    context 'invalid attributes' do
      let(:invalid_cameramodel) { create(:invalid_cameramodel) }
      it 'not increase Cameramodel after create' do
        expect { post :create, cameramodel: attributes_for(:invalid_cameramodel) }.to_not change(Cameramodel, :count)
      end

      it 'render new view' do
        post :create, cameramodel: attributes_for(:invalid_cameramodel)
        expect(response).to render_template :new
      end
    end
  end

  describe 'PATCH update' do
    sign_in_admin
    context 'valid attributes' do
      it 'return cameramodel to @cameramodel' do
        patch :update, id: cameramodel, cameramodel: attributes_for(:cameramodel)
        expect(assigns(:cameramodel)).to eq cameramodel
      end

      it 'changes cameramodel attributes' do
        patch :update, id: cameramodel, cameramodel: { name: 'new name', brand: 'new brand' }
        cameramodel.reload
        expect(cameramodel.name).to eq 'new name'
        expect(cameramodel.brand).to eq 'new brand'
      end

      it 'redirect to show view' do
        patch :update, id: cameramodel, cameramodel: attributes_for(:cameramodel)
        expect(response).to redirect_to cameramodel
      end
    end

    context 'invalid attributes' do
      let(:cameramodel1) { create(:cameramodel, name: '18-55') }
      before { patch :update, id: cameramodel1, cameramodel: { name: nil, brand: nil } }
      it 'doesnt change cameramodel attributes' do
        expect(cameramodel1.name).to eq '18-55'
        expect(cameramodel1.brand).to eq 'SONY'
      end

      it 'render edit view' do
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE destroy' do
    sign_in_admin
    let!(:cameramodel1) { create(:cameramodel) }
    # before { delete :destroy, id: cameramodel1 }
    it 'delete cameramodel' do
      expect { delete :destroy, id: cameramodel1 }.to change(Cameramodel, :count).by(-1)
    end

    it 'redirect to cameramodel/:id' do
      delete :destroy, id: cameramodel1
      expect(response).to redirect_to cameramodels_path
    end
  end
end
