require 'rails_helper'

RSpec.describe CameraitemsController, type: :controller do
  let(:user) { create(:user) }
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }
  let(:cameramodel) { create(:cameramodel) }
  let(:cameraitem)  { create(:cameraitem, cameramodel: cameramodel, user: user, price: 10_000) }
  let(:cameraitem1) { create(:cameraitem, cameramodel: cameramodel, user: user1, price: 1000) }
  let(:cameraitem2) { create(:cameraitem, cameramodel: cameramodel, user: user2) }

  describe 'GET index' do
    before { get :index }
    it 'return array of all @cameraitems' do
      expect(assigns(:cameraitems)).to match_array([cameraitem1, cameraitem2])
    end
    it 'render index view' do
      expect(response).to render_template :index
    end
  end

  describe 'GET show' do
    before { get :show, id: cameraitem1 }
    it 'return @cameraitem' do
      expect(assigns(:cameraitem)).to eq cameraitem1
    end

    it 'render show view' do
      expect(response).to render_template :show
    end
  end

  describe 'GET new' do
    sign_in_user
    before { get :new, cameramodel_id: Cameramodel.first }
    it 'new cameraitem to @cameraitem' do
      expect(assigns(:cameraitem)).to be_a_new(Cameraitem)
    end

    it 'render new view' do
      expect(response).to render_template :new
    end
  end

  describe 'GET edit' do
    sign_in_user
    before { get :edit, id: cameraitem1 }
    it 'cameraitem to @cameraitem' do
      expect(assigns(:cameraitem)).to eq cameraitem1
    end
  end

  describe 'POST create' do
    before { sign_in(user) }
    context 'valid attributes' do
      it 'increase Cameraitem after create' do
        expect { post :create, id: cameraitem, cameraitem: attributes_for(:cameraitem) }.to change(Cameraitem, :count).by(1)
      end
    end

    context 'invalid attributes' do
      let(:invalid_cameraitem) { create(:invalid_cameraitem) }
      it 'not increase Cameraitem after create' do
        expect { post :create, cameraitem: attributes_for(:invalid_cameraitem) }.to_not change(Cameraitem, :count)
      end

      it 'render new view' do
        post :create, cameraitem: attributes_for(:invalid_cameraitem)
        expect(response).to render_template :new
      end
    end
  end

  describe 'PATCH update' do
    before { sign_in(user) }
    context 'valid attributes' do
      it 'return cameraitem to @cameraitem' do
        patch :update, id: cameraitem, cameraitem: attributes_for(:cameraitem)
        expect(assigns(:cameraitem)).to eq cameraitem
      end

      it 'changes cameraitem attributes' do
        patch :update, id: cameraitem, cameraitem: { price: 2000, description: 'old school' }
        cameraitem.reload
        expect(cameraitem.price).to eq 2000
        expect(cameraitem.description).to eq 'old school'
      end

      it 'redirect to show view' do
        patch :update, id: cameraitem, cameraitem: attributes_for(:cameraitem)
        expect(response).to redirect_to cameraitem
      end
    end

    context 'invalid attributes' do
      before { patch :update, id: cameraitem, cameraitem: { price: nil, description: 'asd' } }
      it 'doesnt change cameraitem attributes' do
        expect(cameraitem.price).to eq 10_000
        expect(cameraitem.description).to eq 'description SONY'
      end

      it 'render edit view' do
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:user) { create(:user) }
    let!(:cameraitem) { create(:cameraitem, cameramodel: cameramodel, user: user) }
    it 'delete cameramodel' do
      sign_in(user)
      expect { delete :destroy, id: cameraitem }.to change(Cameraitem, :count).by(-1)
      expect(response).to redirect_to cameraitems_path
    end

    it 'user cant delete another user cameraitem' do
      sign_in(user2)
      expect { delete :destroy, id: cameraitem }.to_not change(cameramodel.cameraitems, :count)
    end
  end
end
