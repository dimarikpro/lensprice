require 'rails_helper'

RSpec.describe AttachesController, type: :controller do
  let(:user) { create(:user) }
  let(:lensmodel) { create(:lensmodel, user: user) }
  let(:lensitem) { create(:lensitem, user: user, lensmodel: lensmodel) }
  let(:q_attach) { create(:attach, attachable: lensmodel) }
  let(:a_attach) { create(:attach, attachable: lensitem) }

  describe 'DELETE #destroy' do
    context 'author of the lensmodel' do
      before { sign_in(q_attach.attachable.user) }
      it 'deletes q_attach' do
        expect { delete :destroy, id: q_attach, format: :js }.to change(Attach, :count).by(-1)
      end
      it 'render template Destroy' do
        delete :destroy, id: q_attach, format: :js
        expect(response).to render_template :destroy
      end
    end
    context 'author of the lensitem' do
      before { sign_in(a_attach.attachable.user) }
      it 'deletes a_attach' do
        expect { delete :destroy, id: a_attach, format: :js }.to change(Attach, :count).by(-1)
      end
      it 'render template Destroy' do
        delete :destroy, id: a_attach, format: :js
        expect(response).to render_template :destroy
      end
    end
  end
end
