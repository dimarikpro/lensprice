require 'rails_helper'

RSpec.describe CommentsController, type: :controller do
  let(:user) { create(:user) }
  let(:user2) { create(:user) }
  let(:lensmodel) { create(:lensmodel, user: user) }
  let(:lensitem) { create(:lensitem, user: user, lensmodel: lensmodel) }
  let(:comment) { create(:comment, user: user, commentable: lensmodel) }
  let(:comment2) { create(:comment, user: user, commentable: lensitem) }
  describe '#POST create' do
    sign_in_user
    it 'loads lensmodel if parent is lensmodel' do
      post :create, comment: attributes_for(:comment), lensmodel_id: lensmodel, format: :js
      expect(assigns(:parent)).to eq lensmodel
    end
    it 'loads lensitem if parent is lensitem' do
      post :create, comment: attributes_for(:comment), lensitem_id: lensitem, format: :js
      expect(assigns(:parent)).to eq lensitem
    end
  end

  describe 'PATCH #update' do
    before { sign_in(user) }
    it 'changes lensitem attributes' do
      patch :update, id: comment, comment: { body: 'new body' }, format: :js
      comment.reload
      expect(comment.body).to eq 'new body'
    end
    it 'render update template' do
      patch :update, id: comment, comment: attributes_for(:comment), format: :js
      expect(response).to render_template :update
    end
  end

  describe 'DELETE #destroy' do
    before { comment }
    it 'deletes comment' do
      sign_in(user)
      expect { delete :destroy, id: comment, format: :js }.to change(Comment, :count).by(-1)
    end
    it 'user cant delete another user lensitem' do
      sign_in(user2)
      expect { delete :destroy, id: comment.id, format: :js }.to_not change(Comment, :count)
    end
  end
end
