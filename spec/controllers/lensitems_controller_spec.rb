require 'rails_helper'

RSpec.describe LensitemsController, type: :controller do
  let(:user) { create(:user) }
  let(:admin) { create(:user, admin: 1) }
  let(:user1) { create(:user) }
  let(:user2) { create(:user) }
  let(:lensmodel) { create(:lensmodel) }
  let(:lensitem)  { create(:lensitem, lensmodel: lensmodel, user: user, price: 10_000) }
  let(:lensitem1) { create(:lensitem, lensmodel: lensmodel, user: user1, price: 1000) }
  let(:lensitem2) { create(:lensitem, lensmodel: lensmodel, user: user2) }

  describe 'GET index' do
    before { get :index }
    it 'return array of all @lensitems' do
      expect(assigns(:lensitems)).to match_array([lensitem1, lensitem2])
    end
    it 'render index view' do
      expect(response).to render_template :index
    end
  end

  describe 'GET show' do
    before { get :show, id: lensitem1 }
    it 'return @lensitem' do
      expect(assigns(:lensitem)).to eq lensitem1
    end

    it 'render show view' do
      expect(response).to render_template :show
    end
  end

  describe 'GET new' do
    sign_in_user
    before { get :new, lensmodel_id: Lensmodel.first }
    it 'new lensitem to @lensitem' do
      expect(assigns(:lensitem)).to be_a_new(Lensitem)
    end

    it 'render new view' do
      expect(response).to render_template :new
    end
  end

  describe 'GET edit' do
    sign_in_user
    before { get :edit, id: lensitem1 }
    it 'lensitem to @lensitem' do
      expect(assigns(:lensitem)).to eq lensitem1
    end
  end

  describe 'POST create' do
    before { sign_in(user) }
    context 'valid attributes' do
      it 'increase Lensitem after create' do
        expect { post :create, id: lensitem, lensitem: attributes_for(:lensitem) }.to change(Lensitem, :count).by(1)
      end
    end

    context 'invalid attributes' do
      let(:invalid_lensitem) { create(:invalid_lensitem) }
      it 'not increase Lensitem after create' do
        expect { post :create, lensitem: attributes_for(:invalid_lensitem) }.to_not change(Lensitem, :count)
      end

      it 'render new view' do
        post :create, lensitem: attributes_for(:invalid_lensitem)
        expect(response).to render_template :new
      end
    end
  end

  describe 'PATCH update' do
    before { sign_in(user) }
    context 'valid attributes' do
      it 'return lensitem to @lensitem' do
        patch :update, id: lensitem, lensitem: attributes_for(:lensitem)
        expect(assigns(:lensitem)).to eq lensitem
      end

      it 'changes lensitem attributes' do
        patch :update, id: lensitem, lensitem: { price: 2000, description: 'old school' }
        lensitem.reload
        expect(lensitem.price).to eq 2000
        expect(lensitem.description).to eq 'old school'
      end

      it 'redirect to show view' do
        patch :update, id: lensitem, lensitem: attributes_for(:lensitem)
        expect(response).to redirect_to lensitem
      end
    end

    context 'invalid attributes' do
      before { patch :update, id: lensitem, lensitem: { price: nil, description: 'asd' } }
      it 'doesnt change lensitem attributes' do
        expect(lensitem.price).to eq 10_000
        expect(lensitem.description).to eq 'description SONY'
      end

      it 'render edit view' do
        expect(response).to render_template :edit
      end
    end
  end

  describe 'DELETE destroy' do
    let!(:user) { create(:user) }
    let!(:lensitem) { create(:lensitem, lensmodel: lensmodel, user: user) }
    it 'delete lensmodel' do
      sign_in(user)
      expect { delete :destroy, id: lensitem }.to change(Lensitem, :count).by(-1)
      expect(response).to redirect_to lensitems_path
    end

    it 'user cant delete another user lensitem' do
      sign_in(user2)
      expect { delete :destroy, id: lensitem }.to_not change(lensmodel.lensitems, :count)
    end
  end
end
