require 'rails_helper'

RSpec.describe Cameraitem, type: :model do
  let(:user) { create(:user) }
  let(:cameramodel) { create(:cameramodel) }
  let!(:cameraitem)  { create(:cameraitem, cameramodel: cameramodel, user: user) }
  let!(:cameraitem2) { create(:cameraitem, cameramodel: cameramodel, user: user) }

  describe 'cameramodel quantity' do
    it 'increase cameramodel.quantity after create new cameraitem' do
      expect(cameramodel.quantity).to eq 2
    end
    it 'decrease cameramodel.quantity after destroy cameraitem' do
      cameraitem.destroy
      expect(cameramodel.quantity).to eq 1
      cameraitem.destroy
      expect(cameramodel.quantity).to eq 0
    end
  end
end
