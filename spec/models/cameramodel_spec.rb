require 'rails_helper'

RSpec.describe Cameramodel, type: :model do
  context 'validations' do
    it 'validates presence of body' do
      expect(Cameramodel.new(brand: 'SONY', name: 'a850', mount: 'Minolta A', ff: 1)).to be_valid
    end
  end
end
