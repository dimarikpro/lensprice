require 'rails_helper'

RSpec.describe Lensitem, type: :model do
  # it { should belong_to(:user) }
  # it { should belong_to(:lensmodel) }
  # it { should validate_presence_of(:user_id) }
  # it { should have_many(:comments).dependent(:destroy) }
  # it { should validate_presence_of(:price) }

  let(:user) { create(:user) }
  let(:lensmodel) { create(:lensmodel) }
  let!(:lensitem)  { create(:lensitem, lensmodel: lensmodel, user: user) }
  let!(:lensitem2) { create(:lensitem, lensmodel: lensmodel, user: user) }

  describe 'lensmodel quantity' do
    it 'increase lensmodel.quantity after create new lensitem' do
      expect(lensmodel.quantity).to eq 2
    end
    it 'decrease lensmodel.quantity after destroy lensitem' do
      lensitem.destroy
      expect(lensmodel.quantity).to eq 1
      lensitem.destroy
      expect(lensmodel.quantity).to eq 0
    end
  end
end
