require 'rails_helper'

describe Ability do
  subject(:ability) { Ability.new(user) }

  describe 'for guest' do
    let(:user) { nil }

    it { should be_able_to :read, Lensmodel }
    it { should be_able_to :read, Lensitem }
    it { should be_able_to :read, Cameramodel }
    it { should be_able_to :read, Cameraitem }
    it { should be_able_to :read, Comment }

    it { should_not be_able_to :manage, :all }
  end

  describe 'for admin' do
    let(:user) { create :user, admin: 1 }
    it { should be_able_to :manage, :all }
  end

  describe 'for user' do
    let(:admin)     { create :user, admin: 1 }
    let(:user)      { create :user }
    let(:user2)     { create :user }
    let(:lensmodel) { create(:lensmodel) }
    let(:lensitem1) { create(:lensitem, lensmodel: lensmodel, user: user) }
    let(:lensitem2) { create(:lensitem, lensmodel: lensmodel, user: user2) }
    let(:comment1)  { create(:comment, commentable: lensmodel, user: user) }
    let(:comment2)  { create(:comment, commentable: lensmodel, user: user2) }

    it { should_not be_able_to :manage, :all }
    it { should     be_able_to :read, :all }

    it { should     be_able_to :create, Lensitem }
    it { should     be_able_to :update, lensitem1, user: user }
    it { should_not be_able_to :update, lensitem2, user: user }
    it { should     be_able_to :edit, lensitem1, user: user }
    it { should_not be_able_to :edit, lensitem2, user: user }
    it { should     be_able_to :destroy, lensitem1, user: user }
    it { should_not be_able_to :destroy, lensitem2, user: user }

    it { should     be_able_to :create, Comment }
    it { should     be_able_to :update, comment1, user: user }
    it { should_not be_able_to :update, comment2, user: user }
    it { should     be_able_to :edit, comment1, user: user }
    it { should_not be_able_to :edit, comment2, user: user }
    it { should     be_able_to :destroy, comment1, user: user }
    it { should_not be_able_to :destroy, comment2, user: user }
  end
end
