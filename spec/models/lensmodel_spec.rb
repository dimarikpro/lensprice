require 'rails_helper'

RSpec.describe Lensmodel, type: :model do
  context 'validations' do
    # before { build(:lensmodel) }
    # subject { build(:lensmodel) }
    # it { should have_many(:lensitems)}#.dependent(:destroy) }
    # it { should have_many(:comments)}#.dependent(:destroy) }
    # it { should validate_presence_of(:brand).is_at_most(15) }
    # it { should validate_presence_of(:name).is_at_most(60) }
    # it { should validate_presence_of(:mount).is_at_most(10) }
    # it { should validate_presence_of(:frmin) }
    # it { should validate_presence_of(:frmax) }
    # it { should validate_presence_of(:diafmin) }
    # it { should validate_presence_of(:diafmax) }
    # it { should validate_presence_of(:fix) }
    # it { should validate_presence_of(:ff) }
    # it { should accept_nested_attributes_for :attaches }
    # it { should have_many(:votes).dependent(:destroy) }

    it 'validates presence of body' do
      expect(Lensmodel.new(brand: 'SONY', name: '18-55', mount: 'Minolta A', frmin: 18, frmax: 55, diafmin: 3, diafmax: 5, fix: 0, ff: 0)).to be_valid
    end
  end
end
