FactoryGirl.define do
  # sequence(:name) { |i| "name#{i*10}" }

  factory :cameramodel do
    mount 'A-mount'
    brand 'SONY'
    name 'Alfa 999'
    ff 0
  end
  factory :invalid_cameramodel, class: 'Cameramodel' do
    mount nil
    brand nil
    name nil
  end
end
