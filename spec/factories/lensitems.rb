FactoryGirl.define do
  sequence(:price) { |i| i * 1000 }

  factory :lensitem do
    lensmodel 1
    user 1
    price
    votes_count 0
    description 'description SONY'
    visible 1
    used 1
  end

  factory :invalid_lensitem, class: 'Lensitem' do
    lensmodel
    price nil
    user
  end
end
