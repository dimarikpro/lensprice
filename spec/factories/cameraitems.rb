FactoryGirl.define do
  # sequence(:price) { |i| i*1000 }

  factory :cameraitem do
    cameramodel 1
    user 1
    price 999
    votes_count 0
    description 'description SONY'
    visible 1
    used 1
  end

  factory :invalid_cameraitem, class: 'Cameraitem' do
    cameramodel
    price nil
    user
  end
end
