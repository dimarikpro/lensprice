FactoryGirl.define do
  sequence(:name) { |i| "name#{i}-#{i * 10}" }

  factory :lensmodel do
    mount 'Minolta A'
    brand 'SONY'
    name
    frmin 18
    frmax 55
    diafmin 3.5
    diafmax 4.5
    fix 0
    ff 0
  end
  factory :invalid_lensmodel, class: 'Lensmodel' do
    mount nil
    brand nil
    name nil
  end
end
