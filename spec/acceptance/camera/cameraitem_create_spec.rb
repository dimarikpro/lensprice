require_relative '../acceptance_helper'

feature 'User cameraitem' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:cameramodel) { create(:cameramodel) }
  given(:cameramodel2) { create(:cameramodel) }
  given(:cameraitem)   { create(:cameraitem, user: user, cameramodel: cameramodel) }
  given(:cameraitem2)  { create(:cameraitem, user: user2, cameramodel: cameramodel2) }

  scenario 'Authenticated user create cameraitem' do
    sign_in user
    create(:cameraitem, cameramodel: cameramodel, user: user, description: 'Text 1', price: 9900)
    visit new_cameraitem_path
    select cameramodel.name, from: 'Cameramodel'
    fill_in 'cameraitem[price]', with: '8800'
    check 'cameraitem[used]'
    fill_in 'cameraitem[description]', with: 'описание'
    click_on 'Продать камеру'
    expect(current_path).to eq cameraitem_path(Cameraitem.last)
    expect(page).to have_content '8800'
    expect(page).to have_content 'Описание: описание'
    expect(page).to have_content user.email
    visit cameramodel_path(cameramodel)
    expect(page).to have_content '9900'
    expect(page).to have_content '8800'
  end
end
