require_relative '../acceptance_helper'

feature 'User lensitem' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:lensmodel) { create(:lensmodel) }
  given(:lensmodel2) { create(:lensmodel) }
  given(:lensitem)   { create(:lensitem, user: user, lensmodel: lensmodel) }
  given(:lensitem2)  { create(:lensitem, user: user2, lensmodel: lensmodel2) }

  scenario 'Authenticated user create lensitem' do
    sign_in user
    create(:lensitem, lensmodel: lensmodel, user: user, description: 'Text 1', price: 9900)
    visit new_lensitem_path
    select lensmodel.name, from: 'Lensmodel'
    fill_in 'lensitem[price]', with: '8800'
    check 'lensitem[used]'
    fill_in 'lensitem[description]', with: 'описание'
    click_on 'Продать объектив'
    expect(current_path).to eq lensitem_path(Lensitem.last)
    expect(page).to have_content '8800'
    expect(page).to have_content 'Описание: описание'
    expect(page).to have_content user.email
    visit lensmodel_path(lensmodel)
    expect(page).to have_content '9900'
    expect(page).to have_content '8800'
  end
end
