require_relative '../acceptance_helper'

feature 'Create lensmodel.' do
  given(:user) { create(:user, admin: 1) }
  given(:user2) { create(:user) }
  given(:lensmodel) { create(:lensmodel) }
  given(:lensitem) { create(:lensitem, user: user, lensmodel: lensmodel) }
  given(:lensitem2) { create(:lensitem, user: user2, lensmodel: lensmodel) }
  given(:lensmodels) { create_list(:lensmodel, 5) }

  scenario 'Authenticated user creates lensmodel' do
    sign_in(user)
    visit lensmodels_path
    click_on 'объектива'
    fill_in 'Mount', with: 'Minolta A'
    fill_in 'Brand', with: 'SONY'
    fill_in 'Name', with: '18-70'
    fill_in 'Frmin', with: 18
    fill_in 'Frmax', with: 70
    fill_in 'Diafmin', with: 3.5
    fill_in 'Diafmax', with: 6.3
    check 'Fix'
    check 'Ff'
    check 'Macro'
    check 'Weather'
    click_on 'Добавить объектив'
    expect(page).to have_content 'Lensmodel was successfully created.'
    expect(page).to have_content 'Minolta A SONY 18-70'
    expect(page).to have_content 'Таких объективов в продаже пока НЕТ!'
  end

  scenario 'Non-authenticated user try to create lensmodel' do
    visit new_lensmodel_path
    expect(page).to_not have_content 'Добавить в базу модель объектива'
    expect(page).to have_content 'You need to sign in or sign up before continuing.'
  end

  scenario 'user can view lensmodel list' do
    lensmodels
    visit lensmodels_path
    expect(Lensmodel.count).to eq 5
    lensmodels.each { |q| expect(page).to have_content q.name }
  end

  scenario 'user can view lensmodel and lensitems' do
    lensitem
    visit lensmodel_path(lensmodel)
    expect(page).to have_content lensmodel.name
    expect(page).to have_content lensitem.price
  end

  scenario 'Admin can delete lensmodel' do
    sign_in(user)
    visit lensmodel_path(lensmodel)
    click_on 'Удалить?'
    expect(page).to have_content 'Lensmodel was successfully destroyed.'
  end
end
