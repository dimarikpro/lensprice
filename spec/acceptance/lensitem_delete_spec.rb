require_relative 'acceptance_helper'

feature 'User lensitem' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:lensmodel) { create(:lensmodel) }
  given(:lensmodel2) { create(:lensmodel) }
  given(:lensitem)   { create(:lensitem, user: user, lensmodel: lensmodel) }
  given(:lensitem2)  { create(:lensitem, user: user2, lensmodel: lensmodel2) }

  scenario 'Owner can delete his lensitem' do
    sign_in user
    lensitem
    visit lensitem_path(lensitem)
    expect(page).to have_content 'Удалить?'
    click_on 'Удалить?'
    expect(current_path).to eq lensitems_path
  end

  scenario 'non-auth-user cant delete foreign lensitem' do
    lensitem2
    visit lensitem_path(lensitem2)
    expect(page).to_not have_content 'Удалить?'
  end

  scenario 'User cant delete foreign lensitem' do
    sign_in user
    lensitem2
    visit lensitem_path(lensitem2)
    expect(page).to_not have_content 'Удалить?'
  end
end
