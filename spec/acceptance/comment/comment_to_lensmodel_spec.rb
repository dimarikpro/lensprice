require_relative '../acceptance_helper'

feature 'User create comment to lensmodel' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:lensmodel) { create(:lensmodel) }
  given(:lensmodel2) { create(:lensmodel) }
  given!(:lensitem) { create(:lensitem, user: user, lensmodel: lensmodel) }
  given(:lensitem2) { create(:lensitem, user: user2, lensmodel: lensmodel2) }

  background do
    sign_in(user)
    visit lensmodel_path(lensmodel)
  end

  scenario 'User try to create invalid comment', js: true do
    click_on 'Комментировать'
    expect(current_path).to eq lensmodel_path(lensmodel)
    within '.comment-errors' do
      expect(page).to have_content 'Body is too short (minimum is 2 characters)'
    end
  end

  scenario 'Authenticated user create comment', js: true do
    fill_in 'Comment', with: 'text text'
    click_on 'Комментировать'
    expect(current_path).to eq lensmodel_path(lensmodel)
    within '.comments' do
      expect(page).to have_content 'text text'
    end
  end

  scenario 'Authenticated user add file to comment', js: true do
    fill_in 'Comment', with: 'text text'
    attach_file 'File', "#{Rails.root}/spec/spec_helper.rb"
    click_on 'Комментировать'
    within '.comments .comment#comment_1' do
      expect(page).to have_link '(spec_helper.rb)', href: '/uploads/attachment/file/1/spec_helper.rb'
    end
    visit lensmodel_path(lensmodel)
    within '.comments .comment#comment_1' do
      click_on 'Edit?'
      expect(page).to have_link 'Удалить файл spec_helper.rb ?'
    end
  end

  scenario 'Authenticated user delete file from comment', js: true do
    fill_in 'Comment', with: 'text text'
    attach_file 'File', "#{Rails.root}/spec/spec_helper.rb"
    click_on 'Комментировать'
    visit lensmodel_path(lensmodel)
    within '.comments .comment#comment_1' do
      click_on 'Edit?'
      expect(page).to have_link '(spec_helper.rb)', href: '/uploads/attachment/file/1/spec_helper.rb'
      expect(page).to have_link 'Удалить файл spec_helper.rb ?'
      click_on 'Удалить файл spec_helper.rb ?'
    end
    within '.comments .comment#comment_1' do
      expect(page).to_not have_link '(spec_helper.rb)', href: '/uploads/attachment/file/1/spec_helper.rb'
      expect(page).to_not have_link 'Удалить файл spec_helper.rb ?'
    end
  end
end
