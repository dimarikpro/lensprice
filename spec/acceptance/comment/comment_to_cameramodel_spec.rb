require_relative '../acceptance_helper'

feature 'User create comment to cameramodel' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:cameramodel) { create(:cameramodel) }
  given(:cameramodel2) { create(:cameramodel) }
  given!(:cameraitem) { create(:cameraitem, user: user, cameramodel: cameramodel) }
  given(:cameraitem2) { create(:cameraitem, user: user2, cameramodel: cameramodel2) }

  background do
    sign_in(user)
    visit cameramodel_path(cameramodel)
  end

  scenario 'User try to create invalid comment', js: true do
    click_on 'Комментировать'
    expect(current_path).to eq cameramodel_path(cameramodel)
    within '.comment-errors' do
      expect(page).to have_content 'Body is too short (minimum is 2 characters)'
    end
  end

  scenario 'Authenticated user create comment', js: true do
    fill_in 'Comment', with: 'text text'
    click_on 'Комментировать'
    expect(current_path).to eq cameramodel_path(cameramodel)
    within '.comments' do
      expect(page).to have_content 'text text'
    end
  end

  scenario 'Authenticated user add file to comment', js: true do
    fill_in 'Comment', with: 'text text'
    attach_file 'File', "#{Rails.root}/spec/spec_helper.rb"
    click_on 'Комментировать'
    within '.comments .comment#comment_1' do
      expect(page).to have_link '(spec_helper.rb)', href: '/uploads/attachment/file/1/spec_helper.rb'
    end
    visit cameramodel_path(cameramodel)
    within '.comments .comment#comment_1' do
      click_on 'Edit?'
      expect(page).to have_link 'Удалить файл spec_helper.rb ?'
    end
  end

  scenario 'Authenticated user delete file from comment', js: true do
    fill_in 'Comment', with: 'text text'
    attach_file 'File', "#{Rails.root}/spec/spec_helper.rb"
    click_on 'Комментировать'
    visit cameramodel_path(cameramodel)
    within '.comments .comment#comment_1' do
      click_on 'Edit?'
      expect(page).to have_link '(spec_helper.rb)', href: '/uploads/attachment/file/1/spec_helper.rb'
      expect(page).to have_link 'Удалить файл spec_helper.rb ?'
      click_on 'Удалить файл spec_helper.rb ?'
    end
    within '.comments .comment#comment_1' do
      expect(page).to_not have_link '(spec_helper.rb)', href: '/uploads/attachment/file/1/spec_helper.rb'
      expect(page).to_not have_link 'Удалить файл spec_helper.rb ?'
    end
  end
end
