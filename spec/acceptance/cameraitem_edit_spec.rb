require_relative 'acceptance_helper'

feature 'User cameraitem' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:cameramodel) { create(:cameramodel) }
  given(:cameramodel2) { create(:cameramodel) }
  given(:cameraitem)   { create(:cameraitem, user: user, cameramodel: cameramodel) }
  given(:cameraitem2)  { create(:cameraitem, user: user2, cameramodel: cameramodel2) }

  scenario 'Owner can edit his cameraitem' do
    sign_in user
    cameraitem
    visit cameraitem_path(cameraitem)
    expect(page).to have_content 'Редактировать?'
    click_on 'Редактировать?'
    expect(current_path).to eq edit_cameraitem_path(cameraitem)
  end

  scenario 'non-auth-user cant edit foreign cameraitem' do
    cameraitem2
    visit cameraitem_path(cameraitem2)
    expect(page).to_not have_content 'Редактировать?'
  end

  scenario 'User cant edit foreign cameraitem' do
    sign_in user
    cameraitem2
    visit cameraitem_path(cameraitem2)
    expect(page).to_not have_content 'Редактировать?'
  end
end
