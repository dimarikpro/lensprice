require_relative 'acceptance_helper'

feature 'User create comment to lensmodel' do
  given(:user)       { create(:user) }
  given(:user2)      { create(:user) }
  given!(:lensmodel) { create(:lensmodel) }
  given(:lensmodel2) { create(:lensmodel) }
  given!(:lensitem) { create(:lensitem, user: user, lensmodel: lensmodel) }
  given(:lensitem2) { create(:lensitem, user: user2, lensmodel: lensmodel2) }

  background { sign_in(user) }

  scenario 'User try to create invalid comment', js: true do
    pending 'работает - но валит тест из-за js'
    visit lensmodel_path(lensmodel)
    click_on 'Комментировать'
    expect(current_path).to eq lensmodel_path(lensmodel)
    within '.comment-errors' do
      expect(page).to have_content 'Body is too short (minimum is 2 characters)'
    end
  end

  scenario 'Authenticated user create comment', js: true do
    pending 'работает - но валит тест из-за js'
    visit lensmodel_path(lensmodel)
    fill_in 'Comment', with: 'text text'
    click_on 'Комментировать'
    expect(current_path).to eq lensmodel_path(lensmodel)
    within '.comments' do
      expect(page).to have_content 'text text'
    end
  end
end
