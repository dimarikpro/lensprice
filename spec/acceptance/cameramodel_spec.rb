require_relative 'acceptance_helper'

feature 'Create cameramodel.' do
  given(:user) { create(:user) }
  given(:user2) { create(:user) }
  given(:cameramodel) { create(:cameramodel) }
  given(:cameraitem) { create(:cameraitem, user: user, cameramodel: cameramodel) }
  given(:cameraitem2) { create(:cameraitem, user: user2, cameramodel: cameramodel) }
  given(:cameramodels) { create_list(:cameramodel, 5) }

  scenario 'Authenticated user creates cameramodel' do
    sign_in(user)
    visit cameramodels_path
    click_on 'камеры'
    fill_in 'Mount', with: 'Minolta A'
    fill_in 'Brand', with: 'SONY'
    fill_in 'Name', with: 'a999'
    check 'Ff'
    check 'Weather'
    click_on 'Добавить камеру'
    expect(page).to have_content 'Cameramodel was successfully created.'
    expect(page).to have_content 'Minolta A SONY a999'
    expect(page).to have_content 'Таких фотоаппаратов в продаже пока НЕТ!'
  end

  scenario 'Non-authenticated user try to create cameramodel' do
    visit new_cameramodel_path
    expect(page).to_not have_content 'Добавить в базу модель'
    expect(page).to have_content 'You need to sign in or sign up before continuing.'
  end

  scenario 'user can view cameramodel list' do
    cameramodels
    visit cameramodels_path
    expect(Cameramodel.count).to eq 5
    cameramodels.each { |q| expect(page).to have_content q.name }
  end

  scenario 'user can view cameramodel and cameraitems' do
    cameraitem
    visit cameramodel_path(cameramodel)
    expect(page).to have_content cameramodel.name
    expect(page).to have_content cameraitem.price
  end

  scenario 'Admin can delete cameramodel' do
    sign_in(user)
    visit cameramodel_path(cameramodel)
    click_on 'Удалить?'
    expect(page).to have_content 'Cameramodel was successfully destroyed.'
  end
end
