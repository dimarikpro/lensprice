class CreateLensmodels < ActiveRecord::Migration
  def change
    create_table :lensmodels do |t|
      t.string :brand
      t.string :name
      t.string :mount
      t.integer :quantity, default: 0
      t.integer :frmin
      t.integer :frmax
      t.float :diafmin
      t.float :diafmax
      t.string :afdrive
      t.boolean :fix
      t.boolean :ff
      t.boolean :macro
      t.boolean :weather
      t.string :link1
      t.string :link2
      t.timestamps null: false
    end
    add_index :lensmodels, :name
  end
end
