class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :password
      t.string :password_confirmation
      t.string :info
      t.string :region
      t.string :image
      t.string :telnumber
      t.string :role
      t.boolean :visible
      t.integer :votes_count, default: 0
      t.timestamps null: false
    end
  end
end
