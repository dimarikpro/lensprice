class CreateCameramodels < ActiveRecord::Migration
  def change
    create_table :cameramodels do |t|
      t.string :brand
      t.string :name
      t.string :mount
      t.boolean :ff
      t.boolean :weather
      t.integer :quantity, default: 0
      t.string :link1
      t.string :link2
      t.timestamps null: false
    end
  end
end
