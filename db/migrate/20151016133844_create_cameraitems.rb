class CreateCameraitems < ActiveRecord::Migration
  def change
    create_table :cameraitems do |t|
      t.integer :cameramodel_id
      t.integer :user_id
      t.integer :price
      t.integer :votes_count
      t.string :description
      t.boolean :visible, default: true
      t.boolean :used
      t.string :image
      t.string :foto1
      t.string :foto2
      t.timestamps null: false
    end
  end
end
