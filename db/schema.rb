# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151029101328) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attachments", force: :cascade do |t|
    t.string   "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "comment_id"
  end

  add_index "attachments", ["comment_id"], name: "index_attachments_on_comment_id", using: :btree

  create_table "authorizations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "provider"
    t.string   "uid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "authorizations", ["provider", "uid"], name: "index_authorizations_on_provider_and_uid", using: :btree
  add_index "authorizations", ["user_id"], name: "index_authorizations_on_user_id", using: :btree

  create_table "cameraitems", force: :cascade do |t|
    t.integer  "cameramodel_id"
    t.integer  "user_id"
    t.integer  "price"
    t.integer  "votes_count"
    t.string   "description"
    t.boolean  "visible",        default: true
    t.boolean  "used"
    t.string   "image"
    t.string   "foto1"
    t.string   "foto2"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  create_table "cameramodels", force: :cascade do |t|
    t.string   "brand"
    t.string   "name"
    t.string   "mount"
    t.boolean  "ff"
    t.boolean  "weather"
    t.integer  "quantity",   default: 0
    t.string   "link1"
    t.string   "link2"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.text     "body"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree

  create_table "lensitems", force: :cascade do |t|
    t.integer  "lensmodel_id"
    t.integer  "user_id"
    t.integer  "price"
    t.integer  "votes_count"
    t.string   "description"
    t.boolean  "visible",      default: true
    t.boolean  "used"
    t.string   "image"
    t.string   "foto1"
    t.string   "foto2"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "lensmodels", force: :cascade do |t|
    t.string   "brand"
    t.string   "name"
    t.string   "mount"
    t.integer  "quantity",   default: 0
    t.integer  "frmin"
    t.integer  "frmax"
    t.float    "diafmin"
    t.float    "diafmax"
    t.boolean  "afdrive"
    t.boolean  "fix"
    t.boolean  "ff"
    t.boolean  "macro"
    t.boolean  "weather"
    t.string   "link1"
    t.string   "link2"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "lensmodels", ["name"], name: "index_lensmodels_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "password"
    t.string   "password_confirmation"
    t.string   "info"
    t.string   "region"
    t.string   "image"
    t.string   "telnumber"
    t.string   "role"
    t.boolean  "visible"
    t.integer  "votes_count",            default: 0
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "admin"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
  end

  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "authorizations", "users"
end
