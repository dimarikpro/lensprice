Rails.application.routes.draw do
  devise_scope(:user) { get '/users/sign_out' => 'devise/sessions#destroy' }
  devise_for :users, controllers: { omniauth_callbacks: 'omniauth_callbacks' }

  root to: 'lensmodels#index'

  concern :commentable do
    resources :comments
  end

  resources :lensmodels,   concerns: :commentable, shallow: true
  resources :lensitems,    concerns: :commentable
  resources :cameramodels, concerns: :commentable
  resources :cameraitems,  concerns: :commentable
  resources :users,        concerns: :commentable, only: [:index, :show]
  resources :attachments, only: [:destroy]
end
